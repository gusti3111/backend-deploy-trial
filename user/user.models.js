const { sequelize, Sequelize } = require("../db/models");

const user_games = sequelize.define(
  "user_games",
  {
    username: Sequelize.STRING,
    email: Sequelize.STRING,
    password: Sequelize.STRING,
  },
  {
    timestamps: false,
  }
);
class UserModel {
  getAllUser = async () => {
    try {
      const result = await user_games.findAll();
      return result;
    } catch (error) {
      console.log(error);
    }
  };
}

module.exports = new UserModel();
