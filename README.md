# Backend Team 1

This project serves API and database for App Team 1. Backend environment use [Node v18.15.0](https://nodejs.org/docs/latest-v18.x/api/), framework [Express v18.2](https://expressjs.com/), database [ElephantSQL](https://www.elephantsql.com/).

## Getting started
Clone this project with run the following command :
```
git clone git@gitlab.com:team-1-backend/backend-team-1.git
```

## Node Modules
Install the dependecies modules with run the following command :

* express
  ```
    npm install express@4.18.2
  ```
* pg pg-hstore
  ```
    npm install pg@8.11.0 pg-hstore@2.3.4
  ```
* nodemon
  ```
    npm install nodemon@2.0.22
  ```
* dotenv
  ```
    npm install dotenv@16.3.1
  ```
* sequelize
  ```
    npm install sequelize@6.32.1
  ```
* sequelize-cli
  ```
    npm install sequelize-cli@6.6.1
  ```

## Migration Database
Before start the migration,the first is create user and database with open terminal and following the step here :

```
npx sequelize-cli db:migrate
```
## Running the App
Once that's out of the way, open a terminal and run the following command:

```
npm run dev
```