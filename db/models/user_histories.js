'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_histories extends Model {
    static associate(models) {
      user_histories.belongsTo(models.user_games,{as:"player",foreignKey:"userId"});
      user_histories.belongsTo(models.user_rooms,{as:"gameRoom",foreignKey:"roomId"});
    }
  }
  user_histories.init({
    userId: DataTypes.INTEGER,
    status: DataTypes.STRING,
    roomId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'user_histories',
  });
  return user_histories;
};