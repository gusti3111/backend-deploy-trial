'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_games extends Model {
    static associate(models) {
      user_games.hasOne(models.user_bios,{foreignKey:"userId"});
      user_games.hasMany(models.user_histories,{as:"resultGames",foreignKey:"userId"});
      user_games.hasMany(models.user_rooms,{foreignKey:"player1Id"});
      user_games.hasMany(models.user_rooms,{foreignKey:"player2Id"})
    }
  }
  user_games.init({
    username: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'user_games',
  });
  return user_games;
};